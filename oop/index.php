<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Sheep Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>" . "<br>";


$sungokong = new Ape("kera sakti");
echo "Ape Name : " . $sungokong->name . "<br>";
echo $sungokong->yell("Auooo"); // "Auooo"

echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Kodok Name : " . $kodok->name . "<br>";
echo $kodok->jump("hop hop"); // "hop hop"
